/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colloque.metier;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author formation
 */
@Entity
@Table( name = "participant" )
public class Participant {
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy="increment")
    @Column(name="numpers")
    private int num_pers ;
    @Column(name="nom")
    private String nom ;
    
    @Column(name="prenom")
    private String prenom ;

    @Column(name="email")
     private String email ;
       
    @Column(name="date_naiss")
     private String datenaiss ;
        
    @Column(name="organisation")
     private String organisation ;
     
    @Column(name="observations")
     private String observation ;    

public Participant (String nom, String prenom, String email, String datenaiss, String organisation,String observation){
    
    this.nom= nom ;
    this.prenom= prenom ;
    this.email = email ;
    this.datenaiss = datenaiss ;
    this.organisation = organisation ;
    this.observation = observation ;

}
    public int getNum_pers() {
        return num_pers;
    }

    public void setNum_pers(int num_pers) {
        this.num_pers = num_pers;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDatenaiss() {
        return datenaiss;
    }

    public void setDatenaiss(String datenaiss) {
        this.datenaiss = datenaiss;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

}