/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colloque.session;


import colloque.metier.Participant;
import colloque.services.ParticipantService;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;


/**
 *
 * @author formation
 */

public class Main {

    
private static SessionFactory createSessionFactory() {
   
    final StandardServiceRegistry registry = 
            new StandardServiceRegistryBuilder().configure().build();
try{
    return new MetadataSources(registry).buildMetadata().buildSessionFactory();
} catch (Exception e){
        e.printStackTrace();
            StandardServiceRegistryBuilder.destroy(registry);
        
    }
return null ;
}

public static void main(String[]args) {
        
    try (SessionFactory sessionFactory = createSessionFactory()) {
        ParticipantService participantService = new ParticipantService(sessionFactory);
        Participant p = new Participant("Mickey","Mouse", "mickey@gmail.com", "1905-12-12", "Disney", "funny");
        Participant p2 = new Participant("Mimi","Mouse", "mimi@gmail.com", "1905-12-12", "Disney", "awesome");

        participantService.insertParticipant(p);
        participantService.insertParticipant(p2);
        participantService.deleteParticipant(p2);
        sessionFactory.close();
            }   
        }        
    }



